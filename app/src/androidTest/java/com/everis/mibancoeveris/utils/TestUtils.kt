package com.everis.mibancoeveris.utils

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.withId
import com.everis.mibancoeveris.R
import android.support.test.espresso.matcher.ViewMatchers.withText
import com.everis.mibancoeveris.model.Person
import org.hamcrest.Matchers.allOf


object TestUtils {
    fun loginUser(user: String, pass: String) {
        onView(withId(R.id.etMainUser))
                .perform(clearText(), typeText(user), closeSoftKeyboard())

        onView(withId(R.id.etMainPass))
                .perform(clearText(), typeText(pass), closeSoftKeyboard())

        onView(withId(R.id.bMainAccept))
                .perform(click())
    }

    fun registerUSer(person: Person) {
        onView(withId(R.id.etRegisterName))
                .perform(clearText(), typeText(person.name), closeSoftKeyboard())

        onView(withId(R.id.etRegisterLastName))
                .perform(clearText(), typeText(person.lastName), closeSoftKeyboard())

        onView(withId(R.id.sRegisterDocument)).perform(click())
        onView(allOf(withId(android.R.id.text1), withText(person.typeDocument.value))).perform(click())

        onView(withId(R.id.etRegisterDocument))
                .perform(clearText(), typeText(person.document), closeSoftKeyboard())

        onView(withId(R.id.etRegisterAccount))
                .perform(clearText(), typeText(person.account), closeSoftKeyboard())

        onView(withId(R.id.etRegisterUser))
                .perform(clearText(), typeText(person.user), closeSoftKeyboard())

        onView(withId(R.id.etRegisterPass))
                .perform(clearText(), typeText(person.pass), closeSoftKeyboard())

        onView(withId(R.id.bRegisterSave))
                .perform(click())
    }
}