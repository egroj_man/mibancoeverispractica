package com.everis.mibancoeveris

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.everis.mibancoeveris.model.Person
import com.everis.mibancoeveris.utils.TestUtils
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RegisterActivityTest {
    private lateinit var stringToBetyped: String

    @get:Rule
    var activityRule: ActivityTestRule<RegisterActivity>
            = ActivityTestRule(RegisterActivity::class.java)

    @Before
    fun initValidString() {
        // Specify a valid string.
        stringToBetyped = "Espresso"
    }

    @Test
    fun registerUser() {
        val person = Person("name",
                "last name",
                Person.TypeDocument.DNI,
                "123456789",
                "9999",
                "user_register",
                "pass_register")

        TestUtils.registerUSer(person)
    }
}