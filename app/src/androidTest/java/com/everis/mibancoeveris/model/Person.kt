package com.everis.mibancoeveris.model

data class Person (
        val name: String,
        val lastName: String,
        val typeDocument: TypeDocument,
        val document: String,
        val account: String,
        val user: String,
        val pass: String
) {
    enum class TypeDocument(val value: String) {
        DNI("DNI"),
        NIE("NIE"),
        PASSPORT("Pasaporte")
    }
}