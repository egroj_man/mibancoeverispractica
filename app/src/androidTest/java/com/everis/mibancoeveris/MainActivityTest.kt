package com.everis.mibancoeveris

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.everis.mibancoeveris.model.Person
import com.everis.mibancoeveris.utils.TestUtils
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MainActivityTest {
    private lateinit var stringToBetyped: String

    @get:Rule
    var activityRule: ActivityTestRule<MainActivity>
            = ActivityTestRule(MainActivity::class.java)

    @Before
    fun initValidString() {
        // Specify a valid string.
        stringToBetyped = "Espresso"
    }

    @Test
    fun tryRandomUser() {
        TestUtils.loginUser("aldfhasdg", "adfliha skdjf as haeflirug")
    }

    @Test
    fun emptyFields() {
        TestUtils.loginUser("", "")
    }

    @Test
    fun newRegister() {
        onView(withId(R.id.tvMainNewRegister))
                .perform(click())

        val person = Person("name",
                "last name",
                Person.TypeDocument.PASSPORT,
                "123456789",
                "9999",
                "user",
                "pass")

        TestUtils.registerUSer(person)
        TestUtils.loginUser(person.user, person.pass)
    }
}