package com.everis.mibancoeveris

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.everis.mibancoeveris.db.Person
import com.everis.mibancoeveris.extensions.inflate
import kotlinx.android.synthetic.main.user_item.view.*

class UsersAdapter(private val users: List<Person>) : RecyclerView.Adapter<UsersAdapter.UserViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): UserViewHolder {
        return UserViewHolder(parent.inflate(R.layout.user_item))
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bind(users[position])
    }

    class UserViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(person: Person) = with(itemView) {
            tvName.text = person.name
            // Set to the view person's surname
        }
    }
}

