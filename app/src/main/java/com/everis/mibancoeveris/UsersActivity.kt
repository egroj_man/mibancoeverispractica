package com.everis.mibancoeveris

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.everis.mibancoeveris.db.AppDatabase
import com.everis.mibancoeveris.db.Person
import kotlinx.android.synthetic.main.activity_users.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UsersActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        loadUsers()
    }

    private fun loadUsers() {
        GlobalScope.launch {
            val personDao = AppDatabase.getInstance(applicationContext).personDao()
            val persons = personDao.getAll()
            runOnUiThread {
                initAdapter(persons)
            }
        }
    }

    private fun initAdapter(persons: List<Person>) {
        rvUsers?.run {
            layoutManager = LinearLayoutManager(this@UsersActivity)

            /*
             * EXERCISE 3
             *
             * TODO Create UserAdapter and set it to rvUsers's adapter
             */
        }
    }
}
