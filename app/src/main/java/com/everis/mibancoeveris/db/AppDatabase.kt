package com.everis.mibancoeveris.db

import android.arch.persistence.room.Room
import android.content.Context

class AppDatabase {

    companion object {
        @Volatile private var database: Database? = null

        fun getInstance(context: Context): Database {
            return database ?: synchronized(this) {
                database ?: buildDatabase(context).also { database = it }
            }
        }

        private fun buildDatabase(context: Context): Database {
            return Room.databaseBuilder(
                    context.applicationContext,
                    Database::class.java,
                    "Persons.db"
            ).build()
        }
    }
}