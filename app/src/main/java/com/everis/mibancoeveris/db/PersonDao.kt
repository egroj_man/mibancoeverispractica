package com.everis.mibancoeveris.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface PersonDao {
    @Query("SELECT * FROM person")
    fun getAll(): List<Person>

    @Query("SELECT * FROM person WHERE user = :user AND " +
            "pass = :password LIMIT 1")
    fun findByUserAndPassword(user: String, password: String): Person?

    @Insert
    fun insertAll(vararg person: Person)

    @Delete
    fun delete(person: Person)
}
