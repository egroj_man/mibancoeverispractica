package com.everis.mibancoeveris.db

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity
data class Person (
        val name: String,
        val lastName: String,
        val typeDocument: String,
        @PrimaryKey val document: String,
        val account: String,
        val user: String,
        val pass: String
)