package com.everis.mibancoeveris.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase

@Database(entities = [Person::class], version = 1)
abstract class Database : RoomDatabase() {
    abstract fun personDao(): PersonDao
}