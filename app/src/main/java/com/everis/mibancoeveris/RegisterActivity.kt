package com.everis.mibancoeveris

import android.app.Activity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import com.everis.mibancoeveris.db.AppDatabase
import com.everis.mibancoeveris.db.Person
import com.everis.mibancoeveris.db.PersonDao
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class RegisterActivity : Activity() {

    private val texts_doc = arrayOf("DNI", "NIE", "Pasaporte")
    private lateinit var personDao: PersonDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        initDb()
        initSpinner()
        initListeners()
    }

    private fun initDb() {
        GlobalScope.launch {
            personDao = AppDatabase.getInstance(applicationContext).personDao()
        }
    }

    private fun initSpinner() {
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, texts_doc)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sRegisterDocument.adapter = adapter
    }

    private fun initListeners() {
        bRegisterSave?.setOnClickListener {
            saveUser()
            finish()
        }
    }

    private fun saveUser() {
        val name = etRegisterName?.text!!.toString()
        val lastName = etRegisterLastName?.text!!.toString()
        val typeDocument = sRegisterDocument?.selectedItem.toString()
        val document = etRegisterDocument?.text!!.toString()
        val account = etRegisterAccount?.text!!.toString()
        val user = etRegisterUser?.text!!.toString()
        val pass = etRegisterPass?.text!!.toString()

        val person = Person(name, lastName, typeDocument, document, account, user, pass)

        GlobalScope.launch {
            personDao.insertAll(person)
            runOnUiThread {
                val toast = Toast.makeText(this@RegisterActivity, getString(R.string.register_user_saved), Toast.LENGTH_LONG)
                toast.show()
            }
        }
    }
}
