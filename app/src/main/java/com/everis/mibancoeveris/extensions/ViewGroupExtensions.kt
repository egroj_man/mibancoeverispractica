package com.everis.mibancoeveris.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(layout: Int): View {
    return LayoutInflater.from(this.context).inflate(layout, this,false)
}