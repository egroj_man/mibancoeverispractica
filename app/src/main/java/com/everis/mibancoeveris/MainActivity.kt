package com.everis.mibancoeveris

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.everis.mibancoeveris.db.AppDatabase
import com.everis.mibancoeveris.db.Person
import com.everis.mibancoeveris.db.PersonDao
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : Activity() {

    private lateinit var personDao: PersonDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        initDb()
        initListeners()
    }

    private fun initDb() {
        GlobalScope.launch {
            personDao = AppDatabase.getInstance(applicationContext).personDao()
        }
    }

    private fun initListeners() {
        /*
         * EXERCISE 1
         *
         * TODO Create listener of tvMainNewRegister and invoke to launchRegisterActivity
         */

        bMainAccept?.setOnClickListener {
            tryLoging()
        }

        tvUsers.setOnClickListener {
            navigateToUsersActivity()
        }

        /*
         * EXERCISE 2
         *
         * TODO Create two listener in etMainUser and etMainPass
         * TODO enabling and disabling bMainAccept if exist some text in both edit text
         */
    }

    private fun launchRegisterActivity() {
        // Start RegisterActivity.
    }

    private fun tryLoging() {
        val user = etMainUser?.text!!.toString()
        val pass = etMainPass?.text!!.toString()

        GlobalScope.launch {
            val person = personDao.findByUserAndPassword(user, pass)
            runOnUiThread {
                if (person != null) {
                    Toast.makeText(this@MainActivity, getString(R.string.main_login_success), Toast.LENGTH_LONG).show()
                    /*
                     * EXERCISE 4
                     *
                     * TODO call initDetailActivity method
                     */
                    //
                } else {
                    Toast.makeText(this@MainActivity, getString(R.string.main_login_error), Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun initDetailActivity(person: Person) {
        // Navigate to a new activity DetailActivity.
    }

    private fun navigateToUsersActivity() {
        val intent = Intent(this, UsersActivity::class.java)
        startActivity(intent)
    }
}
